<?php
require_once 'traits/DbConnect.php';

abstract class Table
{
    use DbConnect;

    protected $attributes       = [];
    protected $parametersString = 'DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';

    public static function getTableName()
    {

        $tableName = get_called_class();
        $table     = strtolower($tableName);

        return str_replace('table', '', $table) . 's';

    }

    protected function getAttributesString()
    {
        $str = '';
        foreach ($this->attributes as $key => $attribute)
        {
            $str .= $key . ' ' . $attribute . ', ';
        }

        return substr_replace($str, '', -2);
    }

    public function getParametersString()
    {
        return $this->parametersString;
    }

    public function createTable()
    {
        $db = static::getDb();
        try
        {
            $db->exec("CREATE TABLE " . static::getTableName() . ' (' . $this->getAttributesString() . ')' . ' ' . $this->getParametersString());

        }
        catch (Exception $exception)
        {
            echo "Error create table! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }
}