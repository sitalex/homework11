<?php


class PostTable extends Table
{
    protected $attributes       = [
        'id'          => 'INT NOT NULL AUTO_INCREMENT PRIMARY KEY',
        'title'       => 'varchar(255)',
        'description' => 'varchar(255)',
        'date'        => 'datetime',
    ];
    protected $parametersString = "DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    
}