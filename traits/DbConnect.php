<?php


trait DbConnect
{
    protected static $db         = null;
    protected static $dbHost     = 'localhost';
    protected static $dbUser     = 'root';
    protected static $dbPassword = '';
    protected static $dbName     = 'school';

    protected static function setDb()
    {
        try{
            self::$db = new PDO('mysql:host=' . self::$dbHost . ';dbname=' . self::$dbName, self::$dbUser, self::$dbPassword);
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$db->exec('SET NAMES "utf8"');
        }catch (Exception $exception){
            echo "Error connecting to db! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    protected static function getDb()
    {
        if (self::$db == null)
        {
            self::setDb();
        }

        return self::$db;
    }
}